# coding: utf8
# Debian Contributors import/export and basic website functions
#
# Copyright (C) 2013--2014  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals
from django.utils.translation import ugettext as _
from django import http, template, forms
from django.shortcuts import redirect, render, get_object_or_404
from django.views.generic import TemplateView, View
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.core.exceptions import PermissionDenied
from django.utils.timezone import now
import contributors.models as cmodels
import datetime
import json

def contributors(request, year=None):
    today = datetime.date.today()
    if not year:
        year = today.year
    else:
        year = int(year)

    people = cmodels.AggregatedPerson.objects.filter(until__year=year).order_by("user__full_name").select_related("user")
    teams = cmodels.AggregatedSource.objects.filter(until__year=year).order_by("source__name").select_related("source")

    return render(request, "contributors/contributors.html", {
        "year": year,
        "prev_year": year - 1,
        "next_year": year + 1 if year < today.year else None,
        "people": people,
        "teams": teams,
    })

def contributors_flat(request):
    # Query all people
    people = cmodels.AggregatedPerson.objects.order_by("user__full_name").select_related("user")

    return render(request, "contributors/contributors_flat.html", {
        "people": people
    })

def sources_flat(request):
    # Query all people
    teams = cmodels.AggregatedSource.objects.order_by("source__name").select_related("source")

    return render(request, "contributors/sources_flat.html", {
        "teams": teams
    })

class ContributorsNew(TemplateView):
    """
    Show contributors, newest first
    """
    template_name = "contributors/contributors_new.html"

    def get_context_data(self, **kw):
        res = super(ContributorsNew, self).get_context_data(**kw)
        res["days"] = int(self.request.GET.get("days", 30))
        cutoff = now() - datetime.timedelta(days=res["days"])
        res["people"] = cmodels.AggregatedPerson.objects\
                .filter(begin__gte=cutoff)\
                .filter(user__email__endswith="@users.alioth.debian.org")\
                .order_by("-begin")\
                .select_related("user")
        return res

def site_status(request):
    from django.db.models import Count
    sources = cmodels.Source.objects.all().order_by("name")
    stats = {
        "unassociated": list(cmodels.Identifier.objects.filter(user=None).values("type").annotate(count=Count("type")).order_by("type")),
        "associated": list(cmodels.Identifier.objects.filter(user__isnull=False).values("type").annotate(count=Count("type")).order_by("type")),
    }

    return render(request, "contributors/site_status.html", {
        "sources": sources,
        "stats": stats,
    })

#def zobel_dump_vars(request):
#    return render(request, "contributors/zobel_dump_vars.html", {
#        "vars": request.environ.items(),
#    })

class Claim(TemplateView):
    template_name = "contributors/claim.html"

    def get_context_data(self, **kw):
        res = super(Claim, self).get_context_data(**kw)
        if not self.request.user.is_authenticated() or not self.request.user.is_dd:
            raise PermissionDenied
        return res

    def do_association(self, request ,person, type, ident):
        """
        Perform the association of type:ident to person. Returns a message
        about what happened.
        """
        try:
            person = cmodels.User.objects.get(email=person)
        except cmodels.User.DoesNotExist:
            return {
                "msgclass": "warning",
                type: "User {} does not exist".format(person),
            }

        if type == "fpr":
            ident = ident.replace(" ", "")

        try:
            ident = cmodels.Identifier.objects.get(type=type, name=ident)
        except cmodels.Identifier.DoesNotExist:
            return {
                "msgclass": "warning",
                type: "Identifier {}:{} does not exist".format(type, ident),
            }

        if ident.user is None:
            ident.user = person
            ident.save()
            log_entry = "Associated by {} using the web interface".format(request.user.email)
            person.add_log(log_entry)
            ident.add_log(log_entry)
            return {
                "msgclass": "note",
                type: "Identifier {} has been associated with {}".format(
                    ident.name, person.email),
            }
        else:
            return {
                "msgclass": "warning",
                type: "Identifier {} is already associated with {}".format(
                    ident.name, ident.user.email),
            }


    def post(self, request):
        if not request.user.is_authenticated() or not request.user.is_dd:
            raise PermissionDenied
        type = request.POST["type"]
        msg = self.do_association(request, request.POST["person"], type, request.POST["name"])
        context = self.get_context_data(msg=msg)
        return self.render_to_response(context)


class ClaimIdents(View):
    def get(self, request, type):
        if not request.user.is_authenticated() or not request.user.is_dd:
            raise PermissionDenied

        term = request.GET.get("q", None)
        limit = int(request.GET.get("limit", 20))
        idents = []
        if term:
            q = cmodels.Identifier.objects.filter(type=type, name__icontains=term, user__isnull=True)
            if limit > 0:
                q = q[:limit]
            for i in q:
                idents.append(i.name)
            idents.sort(key=lambda x: x.lower())
        res = http.HttpResponse(content_type="application/json")
        json.dump(idents, res, indent=2)
        return res

class ClaimPeople(View):
    def get(self, request):
        if not request.user.is_authenticated() or not request.user.is_dd:
            raise PermissionDenied

        term = request.GET.get("q", None)
        limit = int(request.GET.get("limit", 20))
        users = []
        if term:
            q = cmodels.User.objects.filter(email__icontains=term)
            if limit > 0:
                q = q[:limit]
            for i in q:
                users.append(i.email)
            users.sort(key=lambda x: x.lower())
        res = http.HttpResponse(content_type="application/json")
        json.dump(users, res, indent=2)
        return res
