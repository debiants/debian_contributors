# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contributors', '0005_auto_20150208_1230'),
    ]

    operations = [
        migrations.AlterField(
            model_name='identifier',
            name='name',
            field=models.TextField(max_length=256),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='identifier',
            name='type',
            field=models.CharField(max_length=16, choices=[('login', 'Debian/Alioth login'), ('fpr', 'OpenPGP key fingerprint'), ('email', 'Email address'), ('url', 'URL'), ('wiki', 'Wiki name')]),
            preserve_default=True,
        ),
    ]
