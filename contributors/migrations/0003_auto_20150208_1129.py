# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('contributors', '0002_auto_20150208_1127'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contributiontype',
            name='name',
            field=models.CharField(help_text='Contribution type name, shown on the website and used as an identifier when submitting contribution data.', max_length=32, validators=[django.core.validators.RegexValidator('^[A-Za-z0-9._ -]+$', 'Please use only letters, numbers, spaces, dots, dashes or underscores')]),
            preserve_default=True,
        ),
    ]
