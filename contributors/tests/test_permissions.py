# coding: utf8
# Debian Contributors unit tests
#
# Copyright (C) 2014  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Create your views here.

from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals
from django.test import TestCase
from sources.test_common import *
import datetime
import json

class ContributorsTestCase(SimpleSourceFixtureMixin, DCTestUtilsMixin, TestCase):
    def test_contributors(self):
        tc = DCTestClient(self, 'contributors')
        for user in (self.user_admin, self.user_dd, self.user_alioth, None):
            tc.assertGet(Success(), user)

    def test_contributors_flat(self):
        tc = DCTestClient(self, 'contributors_flat')
        for user in (self.user_admin, self.user_dd, self.user_alioth, None):
            tc.assertGet(Success(), user)

    def test_contributors_new(self):
        tc = DCTestClient(self, 'contributors_new')
        for user in (self.user_admin, self.user_dd, self.user_alioth, None):
            tc.assertGet(Success(), user)

    def test_site_status(self):
        tc = DCTestClient(self, 'site_status')
        for user in (self.user_admin, self.user_dd, self.user_alioth, None):
            tc.assertGet(Success(), user)

    def test_export_sources(self):
        tc = DCTestClient(self, 'contributors_export_sources')

        class CanSeeTokens(DCTestClientCheck):
            def check_result(self, tc):
                tc.fixture.assertEquals(tc.response.status_code, 200)
                data = json.loads(tc.response.content)
                source = [x for x in data if x["name"] == "test"][0]
                tc.fixture.assertIsNotNone(source)
                tc.fixture.assertEquals(source["auth_token"], "testsecret")

        class CannotSeeTokens(DCTestClientCheck):
            def check_result(self, tc):
                tc.fixture.assertEquals(tc.response.status_code, 200)
                data = json.loads(tc.response.content)
                source = [x for x in data if x["name"] == "test"][0]
                tc.fixture.assertIsNotNone(source)
                tc.fixture.assertNotEquals(source["auth_token"], "testsecret")

        tc = DCTestClient(self, 'contributors_export_sources')
        tc.assertGet(CanSeeTokens(), self.user_admin)
        for user in (self.user_dd, self.user_alioth, None):
            tc.assertGet(CannotSeeTokens(), user)

        # Even members of a data source cannot see tokens in export, not even
        # in the sources they admin, to avoid accidentally leaking tokens when
        # giving an export to new developers.
        self.source.admins.add(self.user_dd)
        self.source.admins.add(self.user_alioth)
        for user in (self.user_dd, self.user_alioth, None):
            tc.assertGet(CannotSeeTokens(), user)

    def test_post(self):
        class PostCheck(DCTestClientCheck):
            def __init__(self, token):
                self.token = token
            def get_default_data(self, tc):
                from django.core.files.base import ContentFile
                submission = [ {
                    "id": [ { "type": "login", "id": "enrico" } ],
                    "contributions": [ { "type": "tester" } ]
                }, ]
                return {"source": "test", "auth_token": self.token, "data": ContentFile(json.dumps(submission), "foo.json") }

        class CanPost(PostCheck):
            def check_result(self, tc):
                tc.fixture.assertEquals(tc.response.status_code, 200)
                data = json.loads(tc.response.content)
                tc.fixture.assertEquals(data["code"], 200)
                tc.fixture.assertEquals(data["records_parsed"], 1)
                tc.fixture.assertEquals(data["contributions_processed"], 1)

        class CannotPost(PostCheck):
            def check_result(self, tc):
                tc.fixture.assertEquals(tc.response.status_code, 403)
                data = json.loads(tc.response.content)
                tc.fixture.assertEquals(data["errors"][0], "Authentication token is not correct")
                tc.fixture.assertEquals(data["code"], 403)
                tc.fixture.assertEquals(data["records_parsed"], 0)
                tc.fixture.assertEquals(data["contributions_processed"], 0)

        tc = DCTestClient(self, 'contributors_post')

        # Only POST is allowed
        for user in (self.user_admin, self.user_dd, self.user_alioth, None):
            tc.assertGet(BadMethod(), user)

        # Posting with the right token is allowed by anyone
        for user in (self.user_admin, self.user_dd, self.user_alioth, None):
            tc.assertPost(CanPost("testsecret"), user)

        # Posting with the wrong token is forbidden to anyone
        for user in (self.user_admin, self.user_dd, self.user_alioth, None):
            tc.assertPost(CannotPost("testsecret1"), user)

    def test_claim(self):
        tc = DCTestClient(self, 'contributors_claim')
        for user in (self.user_admin, self.user_dd):
            tc.assertGet(Success(), user)
        for user in (self.user_alioth, None):
            tc.assertGet(Forbidden(), user)

        for user in (self.user_admin, self.user_dd):
            tc.assertPost(Success(), user, data={"type": "email", "name": "enrico@enricozini.org", "person": "enrico@debian.org"})
        for user in (self.user_alioth, None):
            tc.assertPost(Forbidden(), user, data={"type": "email", "name": "enrico@enricozini.org", "person": "enrico@debian.org"})

    def test_claim_idents(self):
        tc = DCTestClient(self, 'contributors_claim_idents', url_kwargs={"type": "email"})
        for user in (self.user_admin, self.user_dd):
            tc.assertGet(Success(), user)
        for user in (self.user_alioth, None):
            tc.assertGet(Forbidden(), user)

    def test_claim_people(self):
        tc = DCTestClient(self, 'contributors_claim_people')
        for user in (self.user_admin, self.user_dd):
            tc.assertGet(Success(), user)
        for user in (self.user_alioth, None):
            tc.assertGet(Forbidden(), user)
