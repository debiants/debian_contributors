# file:///usr/share/doc/fabric/html/tutorial.html

from fabric.api import local, run, sudo, cd, env

ALIOTH_IS_DOWN = False

env.hosts = ["nono.debian.org"]

def prepare_deploy():
    #local("./manage.py test my_app")
    #local("git add -p && git commit")
    local("test `git ls-files -cdmu | wc -l` = 0")
    if ALIOTH_IS_DOWN:
        local("git push nono")
    else:
        local("git push")

def deploy():
    prepare_deploy()
    deploy_dir = "/srv/contributors.debian.org/dc"
    with cd(deploy_dir):
        if ALIOTH_IS_DOWN:
            sudo("git pull --rebase ~enrico/dc.git", user="nm")
        else:
            sudo("git pull --rebase", user="nm")
        sudo("./manage.py collectstatic --noinput", user="nm")
        sudo("./manage.py migrate", user="nm")
        sudo("psql service=contributors -c 'grant select,insert,update,delete on all tables in schema public to nmweb'",
             user="nm")
        sudo("psql service=contributors -c 'grant usage on all sequences in schema public to nmweb'", user="nm")
        sudo("touch dc/wsgi.py", user="nm")
