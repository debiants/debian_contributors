# Debian Contributors website backend
#
# Copyright (C) 2013  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from django.test import TestCase
#from django.utils import unittest
from contributors import models as bmodels
from . import importer
from cStringIO import StringIO
import datetime
import json

class FakeRequest(object):
    def __init__(self):
        self.POST = {}
        self.FILES = {}
        self.META = {}

class ImportTest(TestCase):
    def setUp(self):
        self.source = bmodels.Source(name="test", auth_token="foo")
        self.source.save()

        self.ct_upload = bmodels.ContributionType(
            source=self.source,
            name="upload",
        )
        self.ct_upload.save()

        self.ct_sponsor = bmodels.ContributionType(
            source=self.source,
            name="sponsor",
        )
        self.ct_sponsor.save()

        self.ct_review = bmodels.ContributionType(
            source=self.source,
            name="review",
        )
        self.ct_review.save()

        self.import_data1 = {
            "id": [{
                "type": "login",
                "id": "enrico",
            }],
            "contributions": [
                {
                    "type": "upload",
                    "begin": "2013-08-11",
                    "end": "2013-08-18",
                    "url": "http://example.org/foo/enrico",
                }
            ],
        }

    def make_request(self, data):
        res = FakeRequest()
        res.method = "POST"
        res.POST["source"] = self.source.name
        res.POST["auth_token"] = self.source.auth_token
        res.FILES["data"] = StringIO(json.dumps(data))
        return res

    def test_simple_import(self):
        """
        Test a simple import run
        """
        req = self.make_request([self.import_data1])

        i = importer.Importer()
        self.assertTrue(i.import_request(req))
        self.assertEquals(i.results.code, 200)
        self.assertEquals(i.results.identifiers_skipped, 0)
        self.assertEquals(i.results.contributions_processed, 1)
        self.assertEquals(i.results.contributions_created, 1)
        self.assertEquals(i.results.contributions_updated, 0)
        self.assertEquals(i.results.contributions_skipped, 0)
        self.assertEquals(i.results.records_parsed, 1)
        self.assertEquals(i.results.records_skipped, 0)
        self.assertEquals(i.results.errors, [])

        # See that the identifier was created
        identifier = bmodels.Identifier.objects.get(type="login", name="enrico")
        self.assertEquals(identifier.hidden, False)

        # See that the contribution was created
        contrib = bmodels.Contribution.objects.get(identifier=identifier, type=self.ct_upload)
        self.assertEquals(contrib.begin, datetime.date(2013, 8, 11))
        self.assertEquals(contrib.until, datetime.date(2013, 8, 18))
        self.assertEquals(contrib.url, "http://example.org/foo/enrico")

    def test_empty_import(self):
        req = self.make_request([])
        i = importer.Importer()
        self.assertTrue(i.import_request(req))
        self.assertEquals(i.results.code, 200)
        self.assertEquals(i.results.identifiers_skipped, 0)
        self.assertEquals(i.results.contributions_processed, 0)
        self.assertEquals(i.results.contributions_created, 0)
        self.assertEquals(i.results.contributions_updated, 0)
        self.assertEquals(i.results.contributions_skipped, 0)
        self.assertEquals(i.results.records_parsed, 0)
        self.assertEquals(i.results.records_skipped, 0)

    def test_failed_imports(self):
        """
        Test a simple import run
        """
        req = self.make_request([self.import_data1])
        req.POST["source"] = "fail"
        i = importer.Importer()
        self.assertFalse(i.import_request(req))
        self.assertEquals(i.results.code, 404)

        req = self.make_request([self.import_data1])
        req.POST["auth_token"] = "fail"
        i = importer.Importer()
        self.assertFalse(i.import_request(req))
        self.assertEquals(i.results.code, 403)

class SourceImportTest(TestCase):
    def test_empty_import(self):
        bmodels.Source.import_json([])

    def test_simple_import(self):
        source = bmodels.Source(name="www", desc="www.debian.org", url="http://www.debian.org", auth_token="12345")
        source.save()
        contrib = bmodels.ContributionType(
            source=source,
            name="commit",
            desc="www.debian.org webml commits",
            contrib_desc="Website committer")
        contrib.save()

        sources = bmodels.Source.export_json(with_tokens=True)
        source.delete()
        bmodels.Source.import_json(sources)

        source = bmodels.Source.objects.get(name="www")
        self.assertEquals(source.name, "www")
        self.assertEquals(source.desc, "www.debian.org")
        self.assertEquals(source.url, "http://www.debian.org")
        self.assertEquals(source.auth_token, "12345")
        contrib = source.contribution_types.all()[0]
        self.assertEquals(contrib.name, "commit")
        self.assertEquals(contrib.desc, "www.debian.org webml commits")
        self.assertEquals(contrib.contrib_desc, "Website committer")
