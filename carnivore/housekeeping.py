# Debian Contributors website backend
#
# Copyright (C) 2013  Olivier Berger <obergix@debian.org>
# Copyright (C) 2013  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals
from contributors import models as cmodels
import django_housekeeping as hk
from contributors.housekeeping import AssociateIdentities, UserAccountInfo
from carnivore import keyringanalyzer
import logging

log = logging.getLogger(__name__)

STAGES = ["main", "association"]

class MatchGPGUIDs(hk.Task):
    "Match identifiers to users using UIDs from GPG keyrings"

    DEPENDS = [AssociateIdentities, UserAccountInfo]

    def run_association(self, stage):
        userinfo = self.hk.user_account_info.info
        if False:
            # Pickled cache, useful to skip slow keyring loading when testing code
            try:
                import cPickle as pickle
                with open("cache.pickle") as fd:
                    by_mail = pickle.load(fd)
            except IOError:
                by_key, by_mail = keyringanalyzer.process_keyrings()
                with open("cache.pickle", "w") as fd:
                    pickle.dump(by_mail, fd)
        else:
            by_mail = keyringanalyzer.process_keyrings(uid_whitelist=frozenset(userinfo.keys()))

        # List all unassociated emails
        for i in cmodels.Identifier.objects.filter(type="email", user__isnull=True):
            # See if we have a match
            records = by_mail.get(i.name, None)
            if records is None: continue
            for record in records:
                if record["login"] is None: continue

                # Do the association
                log.info("Associating %s to %s", i.name, record["login"])
                self.hk.associate_identity(i, record["login"], "since it is a UID of key {}".format(record["fpr"]))

