# -*- python -*-
# -*- coding: utf-8 -*-
#
# DDPortfolio service application key ring analyzer tool
# Copyright © 2009, 2010, 2011, 2012 Jan Dittberner <jan@dittberner.info>
# Copyright © 2013 Olivier Berger <olivier.berger@telecom-sudparis.eu>
# Copyright © 2013 Enrico Zini <enrico@debian.org>
#
# This file was part of DDPortfolio service and was adapeted for the Debian Contributors app.
#
# DDPortfolio service is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# DDPortfolio service is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
"""
This is a tool that analyzes GPG and PGP keyrings and stores the
retrieved data in a file database. The tool was inspired by Debian
qa's carnivore.
"""

from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals
from django.conf import settings
import glob
import re
import os
import os.path
import logging
import subprocess
import sys
import email.utils
import logging

log = logging.getLogger(__name__)

KEYRINGS = getattr(settings, "KEYRINGS", "/srv/keyring.debian.org/keyrings")

def _get_keyrings():
    """
    Gets the available keyring files from the keyring directory
    configured in Django settings
    """
    keyrings = glob.glob(os.path.join(KEYRINGS, '*.gpg'))
    #keyrings.extend(glob.glob(os.path.join(KEYRINGS, '*.pgp')))
    keyrings.sort()
    return keyrings

def _parse_uid(uid):
    """
    Parse a uid of the form 'Real Name <email@example.com>' into email
    and realname parts.
    """
    name, mail = email.utils.parseaddr(uid)

    if not name and not mail:
        uid = uid.strip()
        # First, strip comment
        s = uid.find('(')
        e = uid.find(')')
        if s >= 0 and e >= 0:
            uid = uid[:s] + uid[e + 1:]
        s = uid.find('<')
        e = uid.find('>')
        mail = None
        if s >= 0 and e >= 0:
            mail = uid[s + 1:e]
            uid = uid[:s] + uid[e + 1:]
        uid = uid.strip()
        if not mail and uid.find('@') >= 0:
            mail, uid = uid, mail
        return uid, mail
    else:
        return name, mail

def read_keyring(keyring):
    fpr = None
    uids = []
    invalid = False

    logging.debug("get data from %s", keyring)
    proc = subprocess.Popen(["gpg", "-q", "--no-options", "--no-default-keyring",
        "--no-expensive-trust-checks",
        "--keyring", keyring, "--list-keys",
        "--with-colons", "--fixed-list-mode", "--with-fingerprint", "--with-fingerprint"],
        stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = proc.communicate()
    retcode = proc.wait()
    if retcode != 0:
        logging.error("subprocess ended with return code %d and standard error %s", retcode, stderr)

    for line in stdout.splitlines():
        items = line.split(b':')

        # If we are at the start of new key material, produce the old one and
        # start collecting data for the new one
        if items[0] == "pub":
            if not invalid and (fpr or uids): yield fpr, uids
            fpr = None
            uids = []
            invalid = False

        # Skip invalid, expired or revoked entries
        if items[1] and items[1] in b"idre":
            if items[0] == "pub":
                invalid = True
            continue

        # Collect fingerprints and uids
        if items[0] == 'uid':
            uid = items[9].strip()
            name, email = _parse_uid(uid)
            if email:
                uids.append( (name, email) )
        elif items[0] == 'fpr':
            fpr = items[9].strip()

    if not invalid and (fpr or uids): yield fpr, uids

def process_keyrings(uid_whitelist=None):
    """Process the keyrings and store the extracted data in an anydbm
    file."""

    re_login = re.compile(r"([^@]+@(?:debian.org|users.alioth.debian.org))", re.I)

    by_mail = {}
    for keyring in _get_keyrings():
        for fpr, uids in read_keyring(keyring):
            # We are not interested in fingerprints without uids
            if not uids: continue

            info = { "fpr": fpr, "uids": uids, "login": None }
            for name, email in uids:
                uid = email.split(b"@", 1)[0]
                if uid_whitelist and uid not in uid_whitelist: continue

                # Collect key material by email
                by_mail.setdefault(email, []).append(info)

                # Try to map to a login name
                mo = re_login.match(email)
                if mo:
                    login = mo.group(1)
                    if not info["login"]:
                        info["login"] = login
                    elif info["login"] != login:
                        # Report login clashes
                        log.warning("Key %s matches both login %s and %s",
                                    fpr, info["login"], login)

    return by_mail

if __name__ == '__main__':
    logging.basicConfig(stream=sys.stderr, level=logging.INFO)
    print(process_keyrings())
