from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from contributors import views as cviews
import sources.urls as sources_urls

from django.contrib import admin
admin.autodiscover()


urlpatterns = patterns('',
    url(r'^$', cviews.contributors, name="contributors"),

    # Examples:
    # url(r'^$', 'dc.views.home', name='home'),
    # url(r'^dc/', include('dc.foo.urls')),

    url(r'^license/$', TemplateView.as_view(template_name='license.html'), name="root_license"),
    url(r'^about/privacy$', TemplateView.as_view(template_name='privacy.html'), name="root_privacy"),

    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),

    url(r'^contributors/', include('contributors.urls')),
    url(r'^contributor/', include('contributor.urls')),
    url(r'^sources/', include(sources_urls.sources_urls)),
    url(r'^source/', include(sources_urls.source_urls)),
    url(r'^logout/', "django_dacs.views.logout"),
)
