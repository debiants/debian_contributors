# coding: utf8
# Debian Contributors Contributor interface
#
# Copyright (C) 2014  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals
from django.conf.urls import patterns, url
from . import views

urlpatterns = patterns('contributor.views',
    url(r'^(?P<name>[^/]+)$', views.ContributorView.as_view(), name='contributor_detail'),
    url(r'^(?P<name>[^/]+)/save_settings$', 'save_settings',
        name="contributor_save_settings"),
    url(r'^(?P<name>[^/]+)/impersonate$', 'impersonate', name="contributor_impersonate"),
    url(r'^(?P<name>[^/]+)/unclaim$', views.Unclaim.as_view(), name="contributor_unclaim"),
)
