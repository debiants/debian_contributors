# coding: utf8
# Debian Contributors unit tests
#
# Copyright (C) 2014  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Create your views here.

from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals
from django.test import TestCase
from contributors import models as cmodels
from sources.test_common import *
import datetime
import re

class ContributorTestCase(SimpleSourceFixtureMixin, DCTestUtilsMixin, TestCase):
    def test_detail(self):
        class SeesClientCheck(DCTestClientCheck):
            def __init__(self, user_tag):
                self.user_tag = user_tag
            def setup_query(self, tc, **kw):
                self.user = getattr(tc.fixture, "user_" + self.user_tag)
                tc.url_kwargs["name"] = self.user.email

        class SeesAll(SeesClientCheck):
            def check_result(self, tc):
                tc.fixture.assertEquals(tc.response.status_code, 200)
                protected_id = getattr(tc.fixture, "id_{}_home".format(self.user_tag))
                if protected_id.name not in tc.response.content.decode("utf8"):
                    tc.fixture.fail("{} cannot see full details of {}".format(
                        tc.user, protected_id))

        class SeesRedacted(SeesClientCheck):
            def check_result(self, tc):
                tc.fixture.assertEquals(tc.response.status_code, 200)
                protected_id = getattr(tc.fixture, "id_{}_home".format(self.user_tag))
                if protected_id.name in tc.response.content.decode("utf8"):
                    tc.fixture.fail("{} does show in {} when visited as {}".format(
                        protected_id.name, tc.url, tc.user))

        tc = DCTestClient(self, 'contributor_detail')

        # Supervisor can see full details on all
        for u in "admin", "dd", "dd1", "alioth", "alioth1":
            tc.assertGet(SeesAll(u), self.user_admin)

        # Others can only see full details on self
        for u in "admin", "dd", "dd1", "alioth", "alioth1":
            for vu in "dd", "dd1", "alioth", "alioth1":
                visitor = getattr(self, "user_" + vu)
                if u == vu:
                    tc.assertGet(SeesAll(u), visitor)
                else:
                    tc.assertGet(SeesRedacted(u), visitor)

    def test_impersonate(self):
        class ImpersonationClientCheck(DCTestClientCheck):
            def __init__(self, user):
                self.user = user
                self.redirect_url = "http://www.example.com/" + user.email
            def get_default_data(self, tc):
                return {"url": self.redirect_url}
            def setup_query(self, tc, **kw):
                self.session = tc.c.session
                if "impersonate" in self.session:
                    self.session.pop("impersonate", None)
                    self.session.save()
                tc.url_kwargs["name"] = self.user.email

        class IsImpersonating(ImpersonationClientCheck):
            def check_result(self, tc):
                tc.fixture.assertEquals(tc.response.status_code, 302)
                tc.fixture.assertEquals(tc.response["Location"], self.redirect_url)
                if "impersonate" not in tc.c.session:
                    tc.fixture.fail("{} could not impersonate {}".format(tc.user, self.user))
                if tc.c.session["impersonate"] != self.user.email:
                    tc.fixture.fail("{} impersonated {} instead of {}".format(tc.user, tc.c.session["impersonate"], self.user))

        class IsNotImpersonating(ImpersonationClientCheck):
            def setup_query(self, tc, **kw):
                super(IsNotImpersonating, self).setup_query(tc, **kw)
                self.session["impersonate"] = self.user.email
                self.session.save()
            def check_result(self, tc):
                tc.fixture.assertEquals(tc.response.status_code, 302)
                tc.fixture.assertEquals(tc.response["Location"], self.redirect_url)
                tc.fixture.assertNotIn("impersonate", tc.c.session)

        tc = DCTestClient(self, 'contributor_impersonate')

        # Supervisor can impersonate anyone
        for u in "dd", "dd1", "alioth", "alioth1":
            user = getattr(self, "user_" + u)
            tc.assertGet(IsImpersonating(user), self.user_admin)
            # When impersonating, calling impersonate with self will de-impersonate
            tc.assertGet(IsNotImpersonating(user), self.user_admin)

            tc.assertGet(IsImpersonating(user), self.user_admin)
            # When impersonating, calling impersonate as a non-superuser will
            # de-impersonate
            tc.assertGet(IsNotImpersonating(user), self.user_dd)

        # Others cannot impersonate
        for u in "admin", "dd", "dd1", "alioth", "alioth1":
            user = getattr(self, "user_" + u)
            for vu in "dd", "dd1", "alioth", "alioth1":
                visitor = getattr(self, "user_" + vu)
                tc.assertGet(IsNotImpersonating(user), visitor)

    def test_settings(self):
        class UserSettingsCheck(DCTestClientCheck):
            def __init__(self, user_tag):
                self.user_tag = user_tag
            def setup_query(self, tc, **kw):
                self.user = getattr(tc.fixture, "user_" + self.user_tag)
                self.id_user = getattr(tc.fixture, "id_{}_user".format(self.user_tag))
                self.id_home = getattr(tc.fixture, "id_{}_home".format(self.user_tag))
                tc.url_kwargs["name"] = self.user.email

        class CanSee(UserSettingsCheck):
            def __init__(self, user_tag, hide_user, hide_id_user, hide_id_home):
                super(CanSee, self).__init__(user_tag)
                self.hide_user = hide_user
                self.hide_id_user = hide_id_user
                self.hide_id_home = hide_id_home

            def setup_query(self, tc, **kw):
                super(CanSee, self).setup_query(tc, **kw)
                self.user.hidden = self.hide_user
                self.user.save()
                self.id_user.hidden = self.hide_id_user
                self.id_user.save()
                self.id_home.hidden = self.hide_id_home
                self.id_home.save()

            def check_result(self, tc):
                tc.fixture.assertEquals(tc.response.status_code, 200)

                def make_input(tag, checked):
                    return '<input type="checkbox" name="hide_{}" {} class="hide_{}">'.format(
                        tag,
                        'checked="checked"' if checked else '',
                        'user' if tag == 'user' else 'ident')

                content = tc.response.content.decode("utf8")
                inputs = re.findall(r'<input type="checkbox" name="hide_[^>]+>', content)
                tc.fixture.assertEquals(len(inputs), 3)
                tc.fixture.assertEquals(inputs[0], make_input("user", self.hide_user))
                tc.fixture.assertEquals(inputs[1], make_input(self.id_user.pk, self.hide_id_user))
                tc.fixture.assertEquals(inputs[2], make_input(self.id_home.pk, self.hide_id_home))

        class CanSet(UserSettingsCheck):
            def __init__(self, user_tag, hide_user, hide_id_user, hide_id_home, missing_settings_for_visible=True):
                super(CanSet, self).__init__(user_tag)
                self.hide_user = hide_user
                self.hide_id_user = hide_id_user
                self.hide_id_home = hide_id_home
                self.missing_settings_for_visible = missing_settings_for_visible

            def setup_query(self, tc, **kw):
                super(CanSet, self).setup_query(tc, **kw)
                self.user.hidden = False
                self.user.save()
                self.id_user.hidden = False
                self.id_user.save()
                self.id_home.hidden = False
                self.id_home.save()

                if tc.data is None: tc.data = {}
                if self.hide_user: tc.data["hide_user"] = "on"
                if self.hide_id_user: tc.data["hide_{}".format(self.id_user.pk)] = "on"
                if self.hide_id_home: tc.data["hide_{}".format(self.id_home.pk)] = "on"

            def check_result(self, tc):
                from urllib import quote_plus
                tc.fixture.assertEquals(tc.response.status_code, 302)
                tc.fixture.assertEquals(tc.response["Location"],
                                        "http://testserver/contributor/{}".format(quote_plus(self.user.email)))

                saved_user = cmodels.User.objects.get(pk=self.user.pk)
                saved_id_user = cmodels.Identifier.objects.get(pk=self.id_user.pk)
                saved_id_home = cmodels.Identifier.objects.get(pk=self.id_home.pk)
                tc.fixture.assertEquals(saved_user.hidden, self.hide_user)
                tc.fixture.assertEquals(saved_id_user.hidden, self.hide_id_user)
                tc.fixture.assertEquals(saved_id_home.hidden, self.hide_id_home)


        tc = DCTestClient(self, 'contributor_detail')
        tc1 = DCTestClient(self, 'contributor_save_settings')

        for u in "admin", "dd", "dd1", "alioth", "alioth1":
            # Check that each user can see their settings
            visitor = getattr(self, "user_" + u)
            tc.assertGet(CanSee(u, False, False, False), visitor)
            tc.assertGet(CanSee(u, True, True, True), visitor)
            tc.assertGet(CanSee(u, True, False, False), visitor)
            tc.assertGet(CanSee(u, False, True, True), visitor)
            tc.assertGet(CanSee(u, False, True, False), visitor)
            tc.assertGet(CanSee(u, False, False, True), visitor)

            # Check that each user can set and retrieve their settings
            tc1.assertPost(CanSet(u, False, False, False), visitor)
            tc1.assertPost(CanSet(u, True, True, True), visitor)
            tc1.assertPost(CanSet(u, True, False, False), visitor)
            tc1.assertPost(CanSet(u, False, True, True), visitor)
            tc1.assertPost(CanSet(u, False, True, False), visitor)
            tc1.assertPost(CanSet(u, False, False, True), visitor)

    def test_user_visibility(self):
        # Anonymous or non-user, non-admin visiting user page, no hidden
        # settings, cannot see emails or other info

        # Visiting hidden user gives 404 except for same user or admin
        class WhenVisitHiddenUser(DCTestUtilsWhen):
            def setUp(self, fixture):
                super(WhenVisitHiddenUser, self).setUp(fixture)
                tu = self.args["target"]
                tu = getattr(fixture, "user_{}".format(tu))
                self.target = cmodels.User(email="tu{}".format(tu.email), hidden=True)
                self.target.save()
                # If the intention was to visit self, use the new hidden user
                # as a visiting user
                if tu == self.user:
                    self.target.remote_user = "{}:tu{}".format(*self.user.remote_user.rsplit(":", 1))
                    self.user = self.target
                self.url = reverse("contributor_detail", kwargs={ "name": self.target.email })

            def tearDown(self, fixture):
                super(WhenVisitHiddenUser, self).tearDown(fixture)
                self.target.delete()

            def __unicode__(self):
                return "visiting hidden user page at {}".format(self.url)

        for tu in "admin", "dd", "dd1", "alioth", "alioth1":
            self.assertVisit(WhenVisitHiddenUser(user="admin", target=tu), ThenSuccess())

        for u in "dd", "dd1", "alioth", "alioth1":
            for tu in "admin", "dd", "dd1", "alioth", "alioth1":
                if u == tu:
                    self.assertVisit(WhenVisitHiddenUser(user=u, target=tu), ThenSuccess())
                else:
                    self.assertVisit(WhenVisitHiddenUser(user=u, target=tu), ThenNotFound())

        # Visiting hidden identifier gives 404 except for same user or admin
        class WhenVisitHiddenIdentifier(DCTestUtilsWhen):
            def setUp(self, fixture):
                super(WhenVisitHiddenIdentifier, self).setUp(fixture)
                t = self.args["target"]
                self.target_user = getattr(fixture, "user_{}".format(t))
                ti = getattr(fixture, "id_{}_user".format(t))
                self.target = cmodels.Identifier.objects.create(type="email", name="ti{}".format(ti.name), hidden=True, user=self.target_user)
                self.url = reverse("contributor_detail", kwargs={ "name": self.target.name })

            def tearDown(self, fixture):
                super(WhenVisitHiddenIdentifier, self).tearDown(fixture)
                self.target.delete()

            def __unicode__(self):
                return "visiting user page for {} via hidden identifier at {}".format(self.target_user, self.url)

        for tu in "admin", "dd", "dd1", "alioth", "alioth1":
            self.assertVisit(WhenVisitHiddenIdentifier(user="admin", target=tu), ThenSuccess())

        for u in "dd", "dd1", "alioth", "alioth1":
            for tu in "admin", "dd", "dd1", "alioth", "alioth1":
                if u == tu:
                    self.assertVisit(WhenVisitHiddenIdentifier(user=u, target=tu), ThenSuccess())
                else:
                    self.assertVisit(WhenVisitHiddenIdentifier(user=u, target=tu), ThenNotFound())

    def test_unclaim(self):
        tc = DCTestClient(self, 'contributor_unclaim', { "name": self.user_admin.email })
        for user in (self.user_admin, self.user_dd):
            tc.assertGet(Success(), user)
        for user in (self.user_alioth, None):
            tc.assertGet(Forbidden(), user)

        for user in (self.user_admin, self.user_dd):
            tc.assertPost(Redirect("/contributor/enrico%40debian.org"), user, data={"id": self.id_admin_user.pk})
        for user in (self.user_alioth, None):
            tc.assertPost(Forbidden(), user, data={"id": self.id_admin_user.pk})

