from django.contrib.auth import get_user_model

class ImpersonationMiddleware(object):
    def process_request(self, request):
        """
        Implement impersonation if requested in session
        """
        if not request.user.is_authenticated(): return
        if not request.user.is_superuser: return
        key = request.session.get("impersonate", None)
        if key is None: return
        User = get_user_model()
        try:
            new_user = User.objects.get(email=key)
        except User.DoesNotExist:
            return
        request.impersonator = request.user
        request.user = new_user
