# coding: utf8
# Debian Contributors source management functions
#
# Copyright (C) 2013  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals
from django.conf.urls import *
#from sources.views import SourceCreate, SourceUpdate, SourceDelete, ContributionTypeCreate, ContributionTypeUpdate, ContributionTypeDelete
from . import views

sources_urls = patterns('sources.views',
    url(r'^$', views.sources_list, name="source_list"),
    url(r'^add/$', views.SourceCreate.as_view(), name='source_add'),
)

source_urls = patterns('sources.views',
    url(r'^(?P<sname>[^/]+)$', views.source_view, name='source_view'),
    url(r'^(?P<sname>[^/]+)/ctype/(?P<name>[^/]+)/$', views.ctype_view, name='source_ctype_view'),
    url(r'^(?P<sname>[^/]+)/update/$', views.SourceUpdate.as_view(), name='source_update'),
    url(r'^(?P<sname>[^/]+)/delete/$', views.SourceDelete.as_view(), name='source_delete'),
    url(r'^(?P<sname>[^/]+)/delete/contributions/$', views.SourceDeleteContributions.as_view(), name='source_delete_contributions'),
    url(r'^(?P<sname>[^/]+)/backups/$', views.SourceBackupList.as_view(), name='source_backup_list'),
    url(r'^(?P<sname>[^/]+)/backups/(?P<backup_id>[0-9]+)$', views.SourceBackupDownload.as_view(), name='source_backup_download'),
    url(r'^(?P<sname>[^/]+)/members/$', views.source_members, name='source_members'),
    url(r'^(?P<sname>[^/]+)/members/add/$', views.source_members_add, name='source_members_add'),
    url(r'^(?P<sname>[^/]+)/members/delete/$', views.source_members_delete, name='source_members_delete'),
    url(r'^(?P<sname>[^/]+)/ctypes/add/$', views.ContributionTypeCreate.as_view(), name='source_ctype_add'),
    url(r'^(?P<sname>[^/]+)/ctype/(?P<name>[^/]+)/update/$', views.ContributionTypeUpdate.as_view(), name='source_ctype_update'),
    url(r'^(?P<sname>[^/]+)/ctype/(?P<name>[^/]+)/delete/$', views.ContributionTypeDelete.as_view(), name='source_ctype_delete'),
    url(r'^(?P<sname>[^/]+)/ctype/(?P<name>[^/]+)/delete/contributions/$', views.ContributionTypeDeleteContributions.as_view(), name='source_ctype_delete_contributions'),
    url(r'^(?P<sname>[^/]+)/user-settings/$', views.source_user_settings, name='source_user_settings'),
)
