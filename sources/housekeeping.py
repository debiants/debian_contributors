# Debian Contributors maintenance functions
#
# Copyright (C) 2015  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals
import django_housekeeping as hk
from django.conf import settings
from contributors.models import Source
import datetime
import logging

log = logging.getLogger(__name__)

DATA_DIR = getattr(settings, "DATA_DIR", None)

STAGES = ["backup", "main"]

class BackupContributions(hk.Task):
    """
    Make a backup of each source contribution data
    """
    def run_backup(self, stage):
        now = datetime.datetime.utcnow()
        for source in Source.objects.all():
            source.make_backup(backup_time=now)
